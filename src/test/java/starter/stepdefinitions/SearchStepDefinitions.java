package starter.stepdefinitions;
import io.restassured.response.Response;
import starter.apiservices.ProductApi;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import net.thucydides.core.annotations.Steps;

public class SearchStepDefinitions {

    @Steps
    public ProductApi productApi;
    Response response;

    @When ("User sends a request for {string} product")
    public void getProduct (String product) {
        response = productApi.get(product);
    }

    @Then("User should get a response that product is available")
    public void responseStatusOK() {
        productApi.checkResponseStatusCode(200);
    }

    @And("Response should contain requested {string} product")
    public void responseContainProduct(String product) {
        productApi.checkResponseBody(response, product); }

    @Then("User should get a response that product is not available")
    public void responseStatusNotFound() {
        productApi.checkResponseStatusCode(404);
    }
}
