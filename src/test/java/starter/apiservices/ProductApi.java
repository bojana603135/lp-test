package starter.apiservices;

import com.google.gson.Gson;
import io.restassured.response.Response;
import net.serenitybdd.rest.SerenityRest;
import net.thucydides.core.annotations.Step;
import org.junit.Assert;
import starter.models.Product;

import static net.serenitybdd.rest.SerenityRest.restAssuredThat;

public class ProductApi {
    private static final String url = "https://waarkoop-server.herokuapp.com/api/v1/search/demo/";

    @Step("Get request")
    public Response get(String endpoint) {
        return SerenityRest.given()
                .baseUri(url)
                .log().all()
                .contentType("application/json")
                .when()
                .get(endpoint)
                .andReturn();
    }

    @Step("Search Result Status")
    public void checkResponseStatusCode(int statusCode) {
        restAssuredThat(response -> response.statusCode(statusCode));
    }

    @Step("Search Result")
    public void checkResponseBody(Response response, String product) {
        Gson gson = new Gson();
        String[] productsString = response.body().print().replace("[", "").replace("]", "").replace("},{", "}\n{").split("\n");

        for(String productString:productsString){
            Product productJson = gson.fromJson(productString, Product.class);
            if (productJson != null) {
                Assert.assertTrue("Product title is not correct " + productJson.getTitle().toLowerCase(), productJson.getTitle().toLowerCase().contains(product.toLowerCase()));
            } else {
                System.out.println("List of products is empty");
            }
        }
    }
}

