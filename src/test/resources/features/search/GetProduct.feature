Feature: Search for the product

### Please use endpoint GET https://waarkoop-server.herokuapp.com/api/v1/search/demo/{product} for getting the products.
### Available products: "orange", "apple", "pasta", "cola"
### Prepare Positive and negative scenarios

  Scenario Outline: Search for existing product
    When User sends a request for "<product>" product
    Then User should get a response that product is available
    And Response should contain requested "<product>" product
    Examples:
      | product |
      | cola    |
      | orange  |
      | pasta   |
      | apple   |

  Scenario Outline: Search for non-existing product
    When User sends a request for "<product>" product
    Then User should get a response that product is not available
    Examples:
      | product |
      | mango   |
      | car     |



