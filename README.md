# Serenity and Cucamber test project

## About the project
This project is created for Rest Api testing on "https://waarkoop-server.herokuapp.com/api/v1/search/demo/{product}" endpoint.

## Getting started
How to run existing tests from this project, these are necessary steps:

- Install: Java, IntelliJ/Eclipse, Maven
- Clone the project from GitLab (https://gitlab.com/bojana.m.todorovic/lp-test)
- Open the project in the IntelliJ or Eclipse
- Install Cucamber plugin 
- Run Maven Install command
- Run desired tests from GetProduct.feature file

How to add new tests to this project:

- Open the project in the IntelliJ or Eclipse
- If the test is related to search for a product feature, add new scenario in GetProduct.feature file with desired steps
- If the test is for some other feature, create new feature file and then add new scenario there, take GetProduct.feature as a reference
- Use existing test steps where you can
- Add step definition for each new step as in SearchStepDefinitions file
- Add methods needed to support step definitions

## The sample scenario

Scenario Outline: Search for existing product
When User sends a request for "<product>" product
Then User should get a response that product is available
And Response should contain requested "<product>" product
Examples:
| product |
| cola    |
| orange  |
| pasta   |
| apple   |


## The step definition example

@Then("User should get a response that product is available")
public void responseStatusOK() {
productApi.checkResponseStatusCode(200);
}

## Refactored:

- Separated existing Scenario into positive and negative scenarios, added Scenario outline
- Naming convention is changed
- Renamed CarsAPI to ProductApi
- Removed gradle and github related files as not used in this project
- Folder structure

